#pragma once

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/condition_variable.hpp>
#include <twist/support/locking.hpp>

namespace solutions {

class Semaphore {
 public:
  explicit Semaphore(size_t count = 0) {
  }

  void Acquire() {
    // Your code goes here
  }

  void Release() {
    // Your code goes here
  }
};

}  // namespace solutions
