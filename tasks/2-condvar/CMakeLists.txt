cmake_minimum_required(VERSION 3.9)

add_subdirectory(barrier)
add_subdirectory(channel)
add_subdirectory(condvar)
add_subdirectory(semaphore)

