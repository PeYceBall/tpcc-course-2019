#include "scheduler.hpp"

#include <twist/test_framework/test_framework.hpp>

#include <twist/support/random.hpp>
#include <twist/support/time.hpp>

#include <cmath>
#include <ctime>
#include <iostream>
#include <functional>
#include <set>
#include <thread>

// Test utils

class ContextSwitchCounter {
 public:
  ContextSwitchCounter()
    : start_count_{fiber::ExecutionContext::SwitchCount()} {
  }

  size_t Get() const {
    return fiber::ExecutionContext::SwitchCount() - start_count_;
  }

 private:
  size_t start_count_;
};

class CPUTimer {
 public:
  CPUTimer() {
    start_ts_ = std::clock();
  }

  double SecondsElapsed() const {
    auto now_ts = std::clock();
    return 1.0 * (now_ts - start_ts_) / CLOCKS_PER_SEC;
  }

 private:
  std::clock_t start_ts_;
};

// Tests

TEST_SUITE(Scheduler) {
  SIMPLE_TEST(SleepFor) {
    static const auto kDelay = std::chrono::seconds(1);

    auto sleeper = []() {
      twist::Timer timer;
      fiber::SleepFor(kDelay);
      ASSERT_TRUE(timer.Elapsed() > kDelay);
    };

    twist::Timer timer;
    fiber::RunScheduler(sleeper);
    auto elapsed = timer.Elapsed();
    ASSERT_TRUE(elapsed > kDelay);
    ASSERT_TRUE(elapsed < kDelay + std::chrono::milliseconds(100));
  }

  SIMPLE_TEST(ConcurrentSleeps) {
    static const size_t kFibers = 100;

    auto launcher = [&]() {
      for (size_t i = 1; i <= kFibers; ++i) {
        auto sleeper = [i]() {
          fiber::SleepFor(std::chrono::milliseconds(i * 10));
        };
        fiber::Spawn(sleeper);
      }
    };

    twist::Timer timer;
    fiber::RunScheduler(launcher);
    ASSERT_TRUE(timer.Elapsed() < std::chrono::milliseconds(1500));
  }

  SIMPLE_TEST(DontBurnCPU) {
    auto sleeper = []() {
      fiber::SleepFor(std::chrono::seconds(1));
    };

    CPUTimer cpu_timer;
    ContextSwitchCounter switch_counter;

    fiber::RunScheduler(sleeper);

    const auto cpu_time_seconds = cpu_timer.SecondsElapsed();
    const auto switch_count = switch_counter.Get();

    std::cout << "CPU time: " << cpu_time_seconds << " seconds" << std::endl;
    std::cout << "Switch count: " << switch_count << std::endl;

    ASSERT_TRUE(cpu_time_seconds < 0.1);
    ASSERT_TRUE(switch_count < 10);
  }

  SIMPLE_TEST(SleepAndRun) {
    size_t runner_steps = 0;

    auto runner = [&]() {
      twist::Timer timer;
      do {
        ++runner_steps;
        fiber::Yield();
      } while (timer.Elapsed() < std::chrono::seconds(1));
    };

    auto sleeper = [&]() {
      fiber::SleepFor(std::chrono::seconds(1));
      ASSERT_TRUE(runner_steps >= 12345);
    };

    auto main = [&]() {
      fiber::Spawn(runner);
      fiber::Spawn(sleeper);
    };

    fiber::RunScheduler(main);
  }

  SIMPLE_TEST(AwaitSleepFor) {
    bool stop_requested = false;

    auto runner = [&]() {
      while (!stop_requested) {
        fiber::Yield();
      }
    };

    auto sleeper = [&]() {
      fiber::SleepFor(std::chrono::seconds(2));
      stop_requested = true;
    };

    fiber::RunScheduler([&]() {
      fiber::Spawn(runner);
      fiber::Spawn(sleeper);
    });
  }

  SIMPLE_TEST(SleepSort) {
    static const size_t kNumbers = 100;
    std::vector<int> ints;
    for (size_t i = 0; i < kNumbers; ++i) {
      ints.push_back(i);
    }
    auto sorted_ints = ints;

    std::vector<int> sleep_sorted_ints;

    twist::RandomShuffleInplace(ints);

    auto worker = [&](int value) {
      fiber::SleepFor(std::chrono::milliseconds(value));
      sleep_sorted_ints.push_back(value);
    };

    auto launcher = [&]() {
      for (size_t i = 0; i < ints.size(); ++i) {
        fiber::Spawn(std::bind(worker, ints[i]));
      }
    };

    fiber::RunScheduler(launcher);

    ASSERT_EQ(sleep_sorted_ints, sorted_ints);
  }
}

RUN_ALL_TESTS()
