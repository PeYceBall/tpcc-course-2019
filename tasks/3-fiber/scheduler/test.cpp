#include "scheduler.hpp"
#include "mutex.hpp"

#include <twist/test_framework/test_framework.hpp>

#include <twist/support/time.hpp>

#include <atomic>
#include <cmath>
#include <iostream>
#include <set>
#include <thread>

// Test utils

class FiberBarrier {
 public:
  FiberBarrier(size_t threads)
    : threads_(threads), count_(threads) {
  }

  void PassThrough() {
    size_t epoch = epoch_.load();
    size_t curr_count = count_.fetch_sub(1) - 1;
    if (curr_count == 0) {
      count_.store(threads_);
      ++epoch_;
    } else {
      while (epoch == epoch_.load()) {
        fiber::Yield();
      }
    }
  }

 private:
  const size_t threads_;
  std::atomic<size_t> count_;
  std::atomic<size_t> epoch_{0};
};

class ThreadCounter {
 public:
  void Touch() {
    std::lock_guard guard(mutex_);
    ids_.insert(std::this_thread::get_id());
  };

  size_t ThreadCount() {
    std::lock_guard guard(mutex_);
    return ids_.size();
  }

 private:
  std::mutex mutex_;
  std::set<std::thread::id> ids_;
};

void SleepFor(std::chrono::nanoseconds delay) {
  twist::Timer timer;
  do {
    fiber::Yield();
  } while (timer.Elapsed() < delay);
}

class ContextSwitchCounter {
 public:
  ContextSwitchCounter()
    : start_count_{fiber::ExecutionContext::SwitchCount()} {
  }

  size_t Get() const {
    return fiber::ExecutionContext::SwitchCount() - start_count_;
  }

 private:
  size_t start_count_;
};


// Scheduler tests

TEST_SUITE(Scheduler) {
  SIMPLE_TEST(RunOneFiber) {
    std::string message;

    fiber::Scheduler scheduler;
    scheduler.Submit([&message]() {
      message = "Hello";
    });
    scheduler.Shutdown();

    ASSERT_EQ(message, "Hello");
  }

  SIMPLE_TEST(Destructor) {
    fiber::Scheduler scheduler{4};
    scheduler.Submit([]() {});
  }

  SIMPLE_TEST(RunManyFibers) {
    static const size_t kFibers = 128;

    std::atomic<size_t> completed{0};

    auto routine = [&completed]() {
      SleepFor(std::chrono::seconds(1));
      completed.fetch_add(1);
    };

    fiber::Scheduler scheduler{4};
    for (size_t i = 0; i < kFibers; ++i) {
      scheduler.Submit(routine);
    }
    scheduler.Shutdown();

    ASSERT_EQ(completed.load(), kFibers);
  }

  SIMPLE_TEST(Yield) {
    static const int kSteps = 1024;

    FiberBarrier start_barrier{2};
    int finn_steps = 0;
    int jake_steps = 0;

    ContextSwitchCounter switch_counter;

    auto finn = [&]() {
      start_barrier.PassThrough();

      for (int i = 0; i < kSteps; ++i) {
        ++finn_steps;
        fiber::Yield();
        ASSERT_TRUE(std::abs(finn_steps - jake_steps) < 10);
      }
    };

    auto jake = [&]() {
      start_barrier.PassThrough();

      for (int i = 0; i < kSteps; ++i) {
        ++jake_steps;
        fiber::Yield();
        ASSERT_TRUE(std::abs(finn_steps - jake_steps) < 10);
      }
    };

    fiber::Scheduler scheduler{1};

    scheduler.Submit(finn);
    scheduler.Submit(jake);

    scheduler.Shutdown();

    std::cout << "Switch count: " << switch_counter.Get();
    ASSERT_GE(switch_counter.Get(), kSteps * 4);

    ASSERT_EQ(finn_steps, kSteps);
    ASSERT_EQ(jake_steps, kSteps);
  }

  SIMPLE_TEST(ThreadCount) {
    static const size_t kThreads = 11;

    fiber::Scheduler scheduler{kThreads};

    ASSERT_EQ(scheduler.ThreadCount(), kThreads);

    ThreadCounter thread_counter;

    static const size_t kSteps = 30;
    static const size_t kWorkers = 100;

    auto worker = [&]() {
      thread_counter.Touch();
      for (size_t i = 0; i < kSteps; ++i) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        fiber::Yield();
        thread_counter.Touch();
      }
    };

    auto spawner = [&]() {
      for (size_t i = 0; i < kWorkers; ++i) {
        fiber::Spawn(worker);
        fiber::Yield();
        thread_counter.Touch();
      }
    };

    scheduler.Submit(spawner);
    scheduler.Shutdown();

    std::cout << "Scheduler threads detected: " << thread_counter.ThreadCount() << std::endl;

    ASSERT_EQ(thread_counter.ThreadCount(), kThreads);
  }

  SIMPLE_TEST(HardwareConcurrency) {
    fiber::Scheduler scheduler;

    const size_t hw_concurrency = std::thread::hardware_concurrency();
    if (hw_concurrency != 0) {
      ASSERT_EQ(scheduler.ThreadCount(), hw_concurrency);
    } else {
      ASSERT_TRUE(scheduler.ThreadCount() > 0);
    }

    scheduler.Shutdown();
  }

  SIMPLE_TEST(Races) {
    fiber::Scheduler scheduler{4};

    static const size_t kIncrements = 10000;
    static const size_t kFibers = 100;

    FiberBarrier start_barrier{kFibers};

    std::atomic<int> counter = 0;

    auto incrementer = [&]() {
      start_barrier.PassThrough();

      for (size_t i = 0; i < kIncrements; ++i) {
        counter.store(counter.load() + 1);
        fiber::Yield();
      }
    };

    for (size_t i = 0; i < kFibers; ++i) {
      scheduler.Submit(incrementer);
    }

    scheduler.Shutdown();

    std::cout << "Counter: " << counter << std::endl;
    std::cout << "Expected: " << kIncrements * kFibers << std::endl;

    ASSERT_NE(counter, kIncrements * kFibers);
  }

  SIMPLE_TEST(NoRacesInSingleThread) {
    fiber::Scheduler scheduler{1};

    ASSERT_EQ(scheduler.ThreadCount(), 1);

    static const size_t kIncrements = 10000;
    static const size_t kFibers = 100;

    FiberBarrier start_barrier{kFibers};

    int counter = 0;

    auto incrementer = [&]() {
      start_barrier.PassThrough();

      for (size_t i = 0; i < kIncrements; ++i) {
        ++counter;
        fiber::Yield();
      }
    };

    for (size_t i = 0; i < kFibers; ++i) {
      scheduler.Submit(incrementer);
    }

    scheduler.Shutdown();
    ASSERT_EQ(counter, kIncrements * kFibers);
  }

  SIMPLE_TEST(Spawn) {
    static const size_t kWorkers = 1024;
    static const size_t kIncrements = 10000;

    std::atomic<size_t> counter{0};
    std::atomic<size_t> workers_completed{0};

    auto worker = [&]() {
      for (size_t i = 0; i < kIncrements; ++i) {
        counter.store(counter.load() + 1);
        fiber::Yield();
      }
      ++workers_completed;
    };

    auto spawner = [&]() {
      for (size_t i = 0; i < kWorkers; ++i) {
        fiber::Spawn(worker);
        fiber::Yield();
      }
    };

    fiber::Scheduler scheduler{4};

    scheduler.Submit(spawner);
    scheduler.Shutdown();

    std::cout << "Counter value: " << counter.load() << std::endl;

    ASSERT_EQ(workers_completed.load(), kWorkers);
    ASSERT_NE(counter.load(), kIncrements * kWorkers);
  }

  SIMPLE_TEST(Barriers) {
    fiber::Scheduler scheduler;

    const size_t kFibers = 100;
    const size_t kIterations = 1000;

    FiberBarrier barrier{kFibers};

    auto runner = [&]() {
      for (size_t i = 0; i < kIterations; ++i) {
        barrier.PassThrough();
      }
    };

    for (size_t i = 0; i < kFibers; ++i) {
      scheduler.Submit(runner);
    }

    scheduler.Shutdown();
  }
}

class CPUTimer {
 public:
  CPUTimer() {
    start_ts_ = std::clock();
  }

  double SecondsElapsed() const {
    auto now_ts = std::clock();
    return 1.0 * (now_ts - start_ts_) / CLOCKS_PER_SEC;
  }

 private:
  std::clock_t start_ts_;
};

// Mutex tests

TEST_SUITE(Mutex) {
  SIMPLE_TEST(Counting) {
    fiber::Scheduler scheduler{32};

    static const size_t kFibers = 100;
    static const size_t kIncrements = 100000;

    int counter = 0;
    fiber::Mutex mutex;
    FiberBarrier start_barrier{kFibers};

    ContextSwitchCounter switch_counter;

    auto incrementer = [&]() {
      start_barrier.PassThrough();

      for (size_t i = 0; i < kIncrements; ++i) {
        mutex.Lock();
        counter++;
        mutex.Unlock();
      }
    };

    for (size_t i = 0; i < kFibers; ++i) {
      scheduler.Submit(incrementer);
    }

    scheduler.Shutdown();

    std::cout << "Counter value: " << counter << std::endl;
    std::cout << "Fiber context switch count: " << switch_counter.Get() << std::endl;

    ASSERT_EQ(counter, kIncrements * kFibers);
  }

  SIMPLE_TEST(NoBusyWaiting) {
    fiber::Mutex mutex;

    auto holder = [&mutex]() {
      mutex.Lock();
      std::this_thread::sleep_for(std::chrono::seconds(3));
      mutex.Unlock();
    };

    fiber::Scheduler scheduler{10};
    scheduler.Submit(holder);

    auto waiter = [&mutex]() {
      twist::Timer timer;
      mutex.Lock();
      ASSERT_TRUE(timer.Elapsed() > std::chrono::seconds(1));
      mutex.Unlock();
    };

    std::this_thread::sleep_for(std::chrono::seconds(1));

    ContextSwitchCounter switch_counter;
    CPUTimer cpu_timer;

    scheduler.Submit(waiter);
    scheduler.Shutdown();

    const auto switch_count = switch_counter.Get();
    const auto cpu_time_seconds = cpu_timer.SecondsElapsed();

    ASSERT_TRUE_M(switch_count < 42, "Too many context switches for blocking wait on mutex: " << switch_count);
    ASSERT_TRUE_M(cpu_time_seconds < 0.1, "Too much cpu time burning for blocking wait on mutex: " << cpu_time_seconds);
}

  SIMPLE_TEST(DontBlockThread) {
    fiber::Mutex mutex;

    fiber::Scheduler scheduler{1};

    auto holder = [&mutex]() {
      mutex.Lock();

      twist::Timer timer;
      do {
        fiber::Yield();
      } while (timer.Elapsed() < std::chrono::seconds(3));

      mutex.Unlock();
    };

    auto waiter = [&mutex]() {
      mutex.Lock();
      mutex.Unlock();
    };

    static const size_t kWorkerSteps = 100000;

    std::atomic<size_t> worker_steps = 0;

    auto worker = [&worker_steps]() {
      for (size_t i = 0; i < kWorkerSteps; ++i) {
        worker_steps.store(worker_steps.load() + 1);
        fiber::Yield();
      }
    };

    scheduler.Submit(holder);

    std::this_thread::sleep_for(std::chrono::seconds(1));

    scheduler.Submit(waiter);
    scheduler.Submit(worker);

    std::this_thread::sleep_for(std::chrono::seconds(1));

    std::cout << "Worker steps count: " << worker_steps.load() << std::endl;
    ASSERT_TRUE(worker_steps == kWorkerSteps);

    scheduler.Shutdown();
  }
}

RUN_ALL_TESTS()
