#include "rcu_lock.hpp"
#include "hash_table.hpp"

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <twist/threading/stdlike.hpp>

#include <twist/support/random.hpp>

#include <twist/test_utils/count_down_latch.hpp>
#include <twist/test_utils/executor.hpp>

#include <string>
#include <vector>


TEST_SUITE(RCULock) {
  SIMPLE_T_TEST(Synchronize) {
    const size_t kReaders = 17;

    solutions::RCULock rcu_lock;

    std::atomic<size_t> readers_completed{0};
    twist::CountDownLatch readers_latch{kReaders};

    auto reader_routine = [&]() {
      rcu_lock.ReadLock();

      readers_latch.CountDown();

      twist::th::this_thread::sleep_for(
          std::chrono::milliseconds(
              twist::RandomUInteger(100, 1000)));

      readers_completed.fetch_add(1);

      rcu_lock.ReadUnlock();
    };

    twist::ScopedExecutor readers;
    for (size_t i = 0; i < kReaders; ++i) {
      readers.Submit(reader_routine);
    }

    readers_latch.Await();  // Reader in read section now
    rcu_lock.Synchronize();

    ASSERT_EQ(readers_completed.load(), kReaders);

    readers.Join();
  }
}

template <typename Key, typename Value>
using HashTable = solutions::FixedSizeHashTable<Key, Value>;

TEST_SUITE(HashTable) {
  SIMPLE_T_TEST(API) {
    HashTable<std::string, std::string> ht(1);

    std::string value;
    ASSERT_FALSE(ht.Lookup("key", value));
    ASSERT_TRUE(ht.Insert("key", "value"));
    ASSERT_TRUE(ht.Lookup("key", value));
    ASSERT_EQ(value, "value");
    ASSERT_TRUE(ht.Remove("key"));
    ASSERT_FALSE(ht.Lookup("key", value));
  }

  SIMPLE_T_TEST(RemoveAbsent) {
    HashTable<std::string, std::string> ht(3);
    ASSERT_FALSE(ht.Remove("NotFound"));
  }

  SIMPLE_T_TEST(Overwrite) {
    HashTable<std::string, std::string> ht(7);

    ASSERT_TRUE(ht.Insert("key", "old"));
    ASSERT_TRUE(ht.Remove("key"));
    ASSERT_TRUE(ht.Insert("key", "new"));

    std::string value;
    ASSERT_TRUE(ht.Lookup("key", value));
    ASSERT_EQ(value, "new");
  }

  SIMPLE_T_TEST(RepeatedUpdates) {
    HashTable<int, std::string> ht(17);
    ASSERT_TRUE(ht.Insert(1, "One"));
    ASSERT_FALSE(ht.Insert(1, "Two"));

    std::string value;
    ASSERT_TRUE(ht.Lookup(1, value));
    ASSERT_EQ(value, "One");

    ASSERT_TRUE(ht.Remove(1));
    ASSERT_FALSE(ht.Remove(1));

    ASSERT_FALSE(ht.Lookup(1, value));
  }


  SIMPLE_T_TEST(MultipleKeys) {
    HashTable<std::string, int> ht(3);

    const int kRange = 17;

    for (int i = 0; i < kRange; i += 2) {
      ASSERT_TRUE(ht.Insert(std::to_string(i), i));
    }

    for (int i = 0; i < kRange; ++i) {
      int value;
      if (i % 2 == 0) {
        ASSERT_TRUE(ht.Lookup(std::to_string(i), value));
        ASSERT_EQ(value, i);
        ASSERT_TRUE(ht.Remove(std::to_string(i)));
      } else {
        ASSERT_FALSE(ht.Lookup(std::to_string(i), value));
      }
    }

    for (int i = 0; i < kRange; ++i) {
      int value;
      ASSERT_FALSE(ht.Lookup(std::to_string(i), value));
    }
  }
}

RUN_ALL_TESTS()
